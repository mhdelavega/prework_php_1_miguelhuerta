<?php
function diaSemana(int $inputNum):string{ 

    if ($inputNum <= 0) { //número natural
        echo "El número debe ser mayor que 0";
    }
    else if ($inputNum >= 7) {
        $dia = $inputNum % 7; //si el número es igual o mayor de 7, sacamos el número a partir del resto
    }
    else {
        $dia = $inputNum;
    }
    $semana = ['domingo', 'lunes', 'martes', 'miércoles','jueves', 'viernes','sábado'];
    return $semana[$dia];
}

$miNumero = 5; 

echo diaSemana($miNumero);
?>